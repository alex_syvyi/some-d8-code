<?php

namespace Drupal\sponsorship_view_filter_sort\Plugin\views\sort;

use Drupal\views\Plugin\views\sort\SortPluginBase;

/**
 * Sort handler for Sponsor Term Id.
 *
 * @ViewsSort("sponsor_sort")
 */
class SponsorTermIdSort extends SortPluginBase {

  /**
   * {@inheritdoc}
   */
  public function query() {
    $this->ensureMyTable();
    $arg = $this->view->argument['term_node_tid_depth']->argument;
    $definition = [
      'table' => 'node__field_sponsorship',
      'field' => 'entity_id',
      'left_table' => 'node_field_data',
      'left_field' => 'nid',
      'extra' => [
        [
          'field' => 'field_sponsorship_term_id',
          'value' => $arg,
        ],
        [
          'field' => 'deleted',
          'value' => '0',
        ],
      ],
    ];
    $join = \Drupal::service('plugin.manager.views.join')->createInstance('standard', $definition);
    $this->query->addRelationship('node__field_sponsorship', $join, 'sponsor_term_id');
    $orderBySql = <<<ORDER_BY_SQL
      FIELD(node__field_sponsorship.field_sponsorship_term_id, $arg)
ORDER_BY_SQL;
    $this->query->addOrderBy('0', $orderBySql, 'DESC', 'sponsor_term_id');
  }

}
