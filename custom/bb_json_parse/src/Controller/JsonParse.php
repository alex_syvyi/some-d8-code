<?php

namespace Drupal\bb_json_parse\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\bb_json_parse\Interfaces\JsonParserInterface;

/**
 * Returns responses for bb_json_parse module routes.
 */
class JsonParse extends ControllerBase {

  /**
   * The JsonParse service.
   *
   * @var \Drupal\bb_json_parse\Services\JsonParse
   */
  protected $jsonParse;

  /**
   * JsonParse constructor.
   *
   * @param \Drupal\bb_json_parse\Interfaces\JsonParserInterface $jsonParse
   *   The json serialization.
   */
  public function __construct(JsonParserInterface $jsonParse) {
    $this->jsonParse = $jsonParse;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('json_parse.parse_service')
    );
  }

  /**
   * Prepares data to render.
   *
   * @return array
   *   Array of nodes or FALSE if URL couldn't be parsed.
   */
  public function build() {
    $nodes = $this->jsonParse->parse('http://some_secret_url');
    $this->jsonParse->sortByRecent($nodes, 'node_created');
    return [
      '#data' => $nodes,
      '#theme' => 'json_nodes',
    ];
  }

}
